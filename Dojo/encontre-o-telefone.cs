using System;
//http://dojopuzzles.com/problemas/exibe/encontre-o-telefone/
public class Program{
	static char[] letraNum = new char[]{ 'C', 'F', 'I', 'L', 'O', 'S', 'V', 'Z' };
	public static void Main(){
		string linha, numero="";
		char[] chars = new char[30];
		while((linha = Console.ReadLine()) != null){
			chars = linha.ToCharArray();
			foreach(char c in chars) Console.Write(getLetraNum(c)+"");
			Console.WriteLine("");
		}
	}
	static char getLetraNum(char c){
		int nchar = c-'\0';
		if(nchar >= 'A'-'\0' && nchar <= 'Z'-'\0'){
			for(int i=0;i < 8;i++){
				if(nchar <= (letraNum[i]-'\0')){
					c = Convert.ToChar(i+2+'0');
					break;
				}
			}
		}
		return c;
	}
}