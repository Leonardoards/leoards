using System;
//http://dojopuzzles.com/problemas/exibe/troco/
public class Program{
	public static void Main(){
		int[] nota = new int[]{ 1, 5, 10, 50, 100 }; 
		int n = 5, m = 4, qtd;
		double[] moeda = new double[]{ 0.01, 0.05, 0.10, 0.50 };
		double valor = double.Parse(Console.ReadLine());
		valor += 0.001;
		Console.WriteLine("NOTAS:");
		while(n-- != 0){
			qtd = (int) (valor/nota[n]);
			Console.WriteLine("{0} nota(s) de R$ {1}.00", qtd, nota[n]);
			valor -= nota[n]*qtd;
		}
		Console.WriteLine("MOEDAS:");
		while(m-- != 0){
			qtd = (int) (valor/moeda[m]);
			Console.WriteLine("{0} moeda(s) de R$ {1:0.00}", qtd, moeda[m]);
			valor -= moeda[m]*qtd;
		}
	}
}