using System;
//http://dojopuzzles.com/problemas/exibe/produto-escalar-de-vetores/	
public class Program{
	public static void Main(){
		Console.WriteLine("Quantidade de termos que irão compor cada vetor A e B");
		int n = int.Parse(Console.ReadLine()), produtoEscalar = 0;
		int[] a = new int[n], b = new int[n];
		
		for(int i=0;i<n;i++){
			Console.Write("A[{0}] = ", i);
			a[i] = int.Parse(Console.ReadLine());
			Console.WriteLine("");
		}
		for(int i=0;i<n;i++){
			Console.Write("B[{0}] = ", i);
			b[i] = int.Parse(Console.ReadLine());
			Console.WriteLine("");
			
			produtoEscalar += a[i] * b[i];
		}
		Console.WriteLine("Produto escalar de A e B: {0}", produtoEscalar);
	}
}