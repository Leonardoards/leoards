using System;
//http://dojopuzzles.com/problemas/exibe/partida-de-tenis/
public class Jogador{
	private string nome;
	private int pontosSet;
	private int setsVencidos;
	
	public Jogador(string nome){
		this.nome = nome;
		this.pontosSet = 0;
		this.setsVencidos = 0;
	}
	
	public string getNome(){ return nome; }
	public void setNome(string nome){ this.nome = nome; }
	
	public int getPontosSet(){ return pontosSet; }
	public void setPontosSet(int pontosSet){ this.pontosSet = pontosSet; }
	
	public int getSetsVencidos(){ return setsVencidos; }
	public void setSetsVencidos(int setsVencidos){ this.setsVencidos = setsVencidos; }
}

public class Program{
	public static void Main(){
		int rodada = 1;
		int setAtual = 1;
		int[] pontos = new int[]{0,15,30,40};
		bool deuce = false;
		
		Jogador[] jogadores = new Jogador[2];
		Console.WriteLine("Nome do jogador 1");
		jogadores[0] = new Jogador(Console.ReadLine().ToLower());
		
		Console.WriteLine("Nome do jogador 2");
		jogadores[1] = new Jogador(Console.ReadLine().ToLower());
		if(jogadores[0].getNome() == jogadores[1].getNome()) jogadores[1].setNome(jogadores[1].getNome()+2);
		
		Console.Clear();
		string leitura = "";
		int num;
		int vencedor;
		while(jogadores[0].getSetsVencidos() != 2 && jogadores[1].getSetsVencidos() != 2){//Enquanto um jogador não vencer 2 sets.
			if(deuce){//Jogo quando em deuce.
				Console.WriteLine("Placar do set {0} (deuce), rodada {1}:\n"+
							  "Jogador 1, {2}: {3}\n"+
							  "Jogador 2, {4}: {5}\n",
							  setAtual, rodada,
							  jogadores[0].getNome(), (jogadores[0].getPontosSet() == 1) ? "advantage" : "",
							  jogadores[1].getNome(), (jogadores[1].getPontosSet() == 1) ? "advantage" : ""
				);
				Console.WriteLine("Quem foi o vencedor da {0}ª rodada?", rodada);
				leitura = Console.ReadLine().ToLower();
				Int32.TryParse(leitura.Trim(), out num);
				
				if(num == 1 || leitura == jogadores[0].getNome()){
					jogadores[0].setPontosSet(jogadores[0].getPontosSet()+1);//Ponto para o jogador 1.
				}else if(num == 2 || leitura == jogadores[1].getNome()){
					jogadores[1].setPontosSet(jogadores[1].getPontosSet()+1);//Ponto para o jogador 2.
				}else{
					Console.WriteLine("Jogador não reconhicido");
					Console.WriteLine("Aperte enter para continuar");
					Console.ReadLine();
					Console.Clear();
					continue;
				}
				
				if(jogadores[0].getPontosSet() == 1 && jogadores[1].getPontosSet() == 1){//Empate
					//Reiniciar o placar
					jogadores[0].setPontosSet(0);
					jogadores[1].setPontosSet(0);
				}else if(jogadores[0].getPontosSet() == 2 || jogadores[1].getPontosSet() == 2){//Vencedor
					if(jogadores[0].getPontosSet() == 2){//Jogador 1
						jogadores[0].setSetsVencidos(jogadores[0].getSetsVencidos()+1);
						vencedor = 1;
					}else{//Jogador 2
						jogadores[1].setSetsVencidos(jogadores[1].getSetsVencidos()+1);
						vencedor = 2;
					}
					Console.WriteLine("Vencedor do {0}º set é o jogador {1}, {2}!", setAtual, vencedor, jogadores[vencedor-1].getNome());
					Console.WriteLine("Aperte enter para continuar");
					Console.ReadLine();
					
					//Reiniciar o placar e o nº de rodadas
					jogadores[0].setPontosSet(0);
					jogadores[1].setPontosSet(0);
					rodada = 0;
					
					setAtual++;//Próximo set
				}
			}else{//Jogo quando não está em deuce.
				Console.WriteLine("Placar do set {0}, rodada {1}:\n"+
							  "Jogador 1, {2}: {3} pontos\n"+
							  "Jogador 2, {4}: {5} pontos\n",
							  setAtual, rodada,
							  jogadores[0].getNome(), pontos[jogadores[0].getPontosSet()],
							  jogadores[1].getNome(), pontos[jogadores[1].getPontosSet()]
				);
				
				Console.WriteLine("Quem foi o vencedor da {0}ª rodada?", rodada);
				leitura = Console.ReadLine().ToLower();
				
				Int32.TryParse(leitura.Trim(), out num);
				if(num == 1 || leitura == jogadores[0].getNome()){
					jogadores[0].setPontosSet(jogadores[0].getPontosSet()+1);//Ponto para o jogador 1.
				}else if(num == 2 || leitura == jogadores[1].getNome()){
					jogadores[1].setPontosSet(jogadores[1].getPontosSet()+1);//Ponto para o jogador 2.
				}else{
					Console.WriteLine("Jogador não reconhecido, digite o vencedor da rodada novamente");
					Console.WriteLine("Aperte enter para continuar...");
					Console.ReadLine();
					Console.Clear();
					continue;
				}
				
				if(jogadores[0].getPontosSet() == 3 && jogadores[1].getPontosSet() == 3){//Empate
					deuce = true;
					//Reiniciar o placar
					jogadores[0].setPontosSet(0);
					jogadores[1].setPontosSet(0);
				}else if(jogadores[0].getPontosSet() == 4 || jogadores[1].getPontosSet() == 4){//Vencedor
					if(jogadores[0].getPontosSet() == 4){//Jogador 1
						jogadores[0].setSetsVencidos(jogadores[0].getSetsVencidos()+1);
						vencedor = 1;
					}else{//Jogador 2
						jogadores[1].setSetsVencidos(jogadores[1].getSetsVencidos()+1);
						vencedor = 2;
					}
					Console.WriteLine("Vencedor do {0}º set é o jogador {1}, {2}!", setAtual, vencedor, jogadores[vencedor-1].getNome());
					Console.WriteLine("Aperte enter para continuar");
					Console.ReadLine();
					
					//Reiniciar o placar e nº da rodada
					jogadores[0].setPontosSet(0);
					jogadores[1].setPontosSet(0);
					rodada = 0;
					
					setAtual++;//Próximo set
				}
			}
			Console.Clear();
			rodada++;
		}
		int perdedor;
		if(jogadores[0].getSetsVencidos() == 2){
			vencedor = 1;
			perdedor = 2;
		}else{
			vencedor = 2;
			perdedor = 1;
		}
		Console.WriteLine("Jogador {0}, {1} é o vencedor da partida com {2} sets\n"+
						  "contra {3} sets do jogador {4}, {5}!", 
						  vencedor, jogadores[vencedor-1].getNome(), jogadores[vencedor-1].getSetsVencidos(),
						  jogadores[perdedor-1].getSetsVencidos(), perdedor, jogadores[perdedor-1].getNome()
		);
	}
}