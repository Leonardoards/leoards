package com.inteli.calculadora.biblioteca;

import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

public class CustomTextWatcher implements TextWatcher {
    private EditText et1;
    private EditText et2;
    private Button btn1;

    public CustomTextWatcher(EditText et1, EditText et2, Button btn1){
        this.et1 = et1;
        this.et2 = et2;
        this.btn1 = btn1;
    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String str1 = et1.getText().toString().trim();
        String str2 = et2.getText().toString().trim();
        boolean check = !str1.isEmpty() && !str2.isEmpty();

        btn1.setEnabled(check);
        btn1.setTextColor((check) ? Color.parseColor("#191919") : Color.parseColor("#cccccc"));
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
