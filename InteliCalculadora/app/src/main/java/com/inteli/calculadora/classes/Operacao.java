package com.inteli.calculadora.classes;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class Operacao {
    private Context context;

    private double a;
    private char op;
    private double b;

    public Operacao(double a, char op, double b) {
        this.a = a;
        this.op = op;
        this.b = b;
    }

    public Operacao(double a, char op, double b, Context context) {
        this.a = a;
        this.op = op;
        this.b = b;
        this.context = context;
    }

    public String getRes(){
        String res = "";
        switch (op) {
            case '+':
                res = res+(a + b);
                break;
            case '-':
                res = res+(a - b);
                break;
            case '/':
                try {
                    if(b == 0){
                        Toast toast = Toast.makeText(context, "Impossível dividir por zero", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 55);
                        toast.show();

                        return "";
                    }
                    res = res+(a / b);
                }catch (Exception e) {
                    Toast toast = Toast.makeText(context, "Impossível realizar esta divisão", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 50);
                    toast.show();
                    return "";
                }
                break;
            case '*':
                res =  res+(a * b);
                break;
        }
        int len = res.length();
        if(len > 2){
            if(res.substring(len-2).equals(".0")){
                res = res.substring(0, len-2);
            }
        }
        return res;
    }

    public void setA(double a){ this.a = a; }
    public double getA(){ return a; }

    public void setOp(char op){ this.op = op; }
    public char getOp(){ return op; }

    public void setB(double b){ this.b = b; }
    public double getB(){ return b; }

    public void setContext(Context context){ this.context = context; }
    public Context getContext(){ return context; }
}
