package com.inteli.calculadora;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.inteli.calculadora.biblioteca.CustomTextWatcher;
import com.inteli.calculadora.classes.Operacao;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] operacao = {"+", "-", "*", "/"};

        final Spinner spOp = (Spinner) findViewById(R.id.spOp);
        final EditText etNum1 = (EditText) findViewById(R.id.etNum1);
        final EditText etNum2 = (EditText) findViewById(R.id.etNum2);
        final Button btnCalcular = (Button) findViewById(R.id.btnCalcular);
        final TextView lblResultado = (TextView) findViewById(R.id.lblResultado);
        int a, b;

        adapterToSpinner(spOp, operacao);

        lblResultado.setText("");
        etNum1.requestFocus();

        btnCalcular.setEnabled(false);
        btnCalcular.setTextColor(Color.parseColor("#cccccc"));//Cor do texto quando estiver desativado.

        CustomTextWatcher customWatcher = new CustomTextWatcher(etNum1, etNum2, btnCalcular);
        etNum1.addTextChangedListener(customWatcher);
        etNum2.addTextChangedListener(customWatcher);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                double numero1 = Double.parseDouble(etNum1.getText().toString());
                char operador = spOp.getSelectedItem().toString().charAt(0);
                double numero2 = Double.parseDouble(etNum2.getText().toString());
                String saida = new Operacao(numero1, operador, numero2, getApplicationContext()).getRes();

                lblResultado.setText(saida);

                etNum1.setText("");
                etNum2.setText("");
                etNum1.requestFocus();
            }
        });
    }

    private void adapterToSpinner(Spinner spinner, String[] data){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }
}
